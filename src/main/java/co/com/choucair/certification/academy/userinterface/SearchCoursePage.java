package co.com.choucair.certification.academy.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SearchCoursePage extends PageObject {
    public static final Target NAME_COURSE = Target.the("Valida el titulo del curso")
            .located(By.xpath("//h3[@class='coursename']/a"));
    public static final Target BUTTON_UC = Target.the("Selecciona  cursos certificaciones")
            .located(By.xpath("//div[@id='certificaciones']/div/a"));
    public static final Target INPUT_COURSE = Target.the("Buscar el curso")
            .located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("Da click para buscar el curso")
            .located(By.xpath("//form[@id='coursesearch']/fieldset/button"));
    public static final Target SELECT_COURSE = Target.the("Da click para entrar al curso")
            .located(By.xpath("//div[@class='search-results']/div[1]/h4/a"));
}