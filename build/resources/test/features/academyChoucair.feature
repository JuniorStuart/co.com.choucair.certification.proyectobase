#Autor Junior Stuart
  @stories
  Feature:Academy Choucair
    As a testing I want to improve my knowledge and for that I am going to take a course on the choucair academy website
    in the Bancolombia automation resources module
  @scenario1
  Scenario: search for automation course
    Given than Rose wants to learn automation at the academy Choucair
    |strUser| strPassword|
    |TuUsuario| TuClave  |

    When  she search for the course on the Choucair Academy platform
    |strCourse    |
    |Metodologia Bancolombia|
    Then she finds the course called
    |strCourse|
    |Metodología Bancolombia|
